import 'package:yops_challenge/models/models.dart';
import 'package:yops_challenge/actions/actions.dart';
import 'package:yops_challenge/presentation/add_edit_screen.dart';
import 'package:flutter/widgets.dart';
import 'package:yops_challenge/src.dart';
import 'package:flutter_built_redux/flutter_built_redux.dart';

class EditTodo
    extends StoreConnector<AppState, AppActions, Null> {
  final Todo todo;

  EditTodo({this.todo, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context, _, AppActions actions) {
    return new AddEditScreen(
      key: AppKeys.editTodoScreen,
      isEditing: true,
      onSave: (task, note) {
        actions.updateTodoAction(new UpdateTodoActionPayload(
            todo.id,
            (todo.toBuilder()
                  ..task = task
                  ..note = note)
                .build()));
      },
      todo: todo,
    );
  }

  @override
  connect(AppState state) {}
}
