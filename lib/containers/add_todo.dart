import 'package:yops_challenge/models/models.dart';
import 'package:yops_challenge/actions/actions.dart';
import 'package:yops_challenge/presentation/add_edit_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_built_redux/flutter_built_redux.dart';

class AddTodo
    extends StoreConnector<AppState, AppActions, Null> {
  AddTodo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context, Null ignored, AppActions actions) {
    return new AddEditScreen(
        isEditing: false,
        onSave: (String task, String note) {
          actions.addTodoAction(new Todo.builder((b) {
            return b
              ..task = task
              ..note = note;
          }));
        });
  }

  @override
  connect(AppState state) {}
}
