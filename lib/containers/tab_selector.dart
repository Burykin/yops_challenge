import 'package:yops_challenge/models/models.dart';
import 'package:yops_challenge/actions/actions.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:yops_challenge/src.dart';
import 'package:flutter_built_redux/flutter_built_redux.dart';

typedef OnTabsSelected = void Function(int);

class TabSelector
    extends StoreConnector<AppState, AppActions, AppTab> {
  TabSelector({Key key}) : super(key: key);

  @override
  AppTab connect(AppState state) => state.activeTab;

  @override
  Widget build(BuildContext context, AppTab activeTab, AppActions action) {
    return new BottomNavigationBar(
      key: AppKeys.tabs,
      currentIndex: AppTab.toIndex(activeTab),
      onTap: (index) {
        action.updateTabAction(AppTab.fromIndex(index));
      },
      items: AppTab.values.map((tab) {
        return new BottomNavigationBarItem(
          icon: new Icon(
            tab == AppTab.todos ? Icons.list : Icons.show_chart,
            key: tab == AppTab.stats
                ? AppKeys.statsTab
                : AppKeys.todoTab,
          ),
          title: new Text(tab == AppTab.stats
              ? AppLocalizations.of(context).stats
              : AppLocalizations.of(context).todos),
        );
      }).toList(),
    );
  }
}
