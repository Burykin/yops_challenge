library src;
import 'package:built_redux/built_redux.dart';
import 'package:yops_challenge/containers/add_todo.dart';
import 'package:yops_challenge/models/models.dart';
import 'package:yops_challenge/localization.dart';
import 'package:yops_challenge/actions/actions.dart';
import 'package:yops_challenge/reducers/reducers.dart';
import 'package:yops_challenge/middleware/store_todos_middleware.dart';
import 'package:yops_challenge/presentation/home_screen.dart';
import 'package:flutter_built_redux/flutter_built_redux.dart';
import 'package:flutter/material.dart';
import 'package:yops_challenge/src.dart';

void main() {
  runApp(new BuiltReduxApp());
}

class BuiltReduxApp extends StatefulWidget {
  final store = new Store<AppState, AppStateBuilder, AppActions>(
    reducerBuilder.build(),
    new AppState.loading(),
    new AppActions(),
    middleware: [
      createStoreTodosMiddleware(),
    ],
  );

  @override
  State<StatefulWidget> createState() {
    return new BuiltReduxAppState();
  }
}

class BuiltReduxAppState extends State<BuiltReduxApp> {
  Store<AppState, AppStateBuilder, AppActions> store;

  BuiltReduxApp() {}

  @override
  void initState() {
    store = widget.store;

    store.actions.fetchTodosAction();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new ReduxProvider(
      store: store,
      child: new MaterialApp(
        title: new BuiltReduxLocalizations().appTitle,
        theme: AppTheme.theme,
        localizationsDelegates: [
          new AppLocalizationsDelegate(),
          new BuiltReduxLocalizationsDelegate(),
        ],
        routes: {
          Routes.home: (context) {
            return new HomeScreen(key: AppKeys.homeScreen);
          },
          Routes.addTodo: (context) {
            return new AddTodo();
          },
        },
      ),
    );
  }
}
