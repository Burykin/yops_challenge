import 'package:yops_challenge/containers/edit_todo.dart';
import 'package:yops_challenge/models/models.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:yops_challenge/src.dart';

class DetailsScreen extends StatelessWidget {
  final Todo todo;
  final Function onDelete;
  final Function(bool) toggleCompleted;

  DetailsScreen({
    Key key,
    @required this.todo,
    @required this.onDelete,
    @required this.toggleCompleted,
  })
      : super(key: AppKeys.todoDetailsScreen);

  @override
  Widget build(BuildContext context) {
    final localizations = AppLocalizations.of(context);

    return new Scaffold(
      appBar: new AppBar(
        title: new Text(localizations.todoDetails),
        actions: [
          new IconButton(
            tooltip: localizations.deleteTodo,
            icon: new Icon(Icons.delete),
            key: AppKeys.deleteTodoButton,
            onPressed: () {
              onDelete();
              Navigator.pop(context, todo);
            },
          )
        ],
      ),
      body: new Padding(
        padding: new EdgeInsets.all(16.0),
        child: new ListView(
          children: [
            new Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                new Padding(
                  padding: new EdgeInsets.only(right: 8.0),
                  child: new Checkbox(
                    key: AppKeys.detailsTodoItemCheckbox,
                    value: todo.complete,
                    onChanged: toggleCompleted,
                  ),
                ),
                new Expanded(
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      new Padding(
                        padding: new EdgeInsets.only(
                          top: 8.0,
                          bottom: 16.0,
                        ),
                        child: new Text(
                          todo.task,
                          key: AppKeys.detailsTodoItemTask,
                          style: Theme.of(context).textTheme.headline,
                        ),
                      ),
                      new Text(
                        todo.note,
                        key: AppKeys.detailsTodoItemNote,
                        style: Theme.of(context).textTheme.subhead,
                      )
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      floatingActionButton: new FloatingActionButton(
        key: AppKeys.editTodoFab,
        tooltip: localizations.editTodo,
        child: new Icon(Icons.edit),
        onPressed: () {
          Navigator.of(context).push(
            new MaterialPageRoute(
              builder: (context) {
                return new EditTodo(
                  todo: todo,
                );
              },
            ),
          );
        },
      ),
    );
  }
}
