import 'package:yops_challenge/containers/app_loading.dart';
import 'package:yops_challenge/containers/todo_details.dart';
import 'package:yops_challenge/models/models.dart';
import 'package:yops_challenge/presentation/todo_item.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:yops_challenge/src.dart';

class TodoList extends StatelessWidget {
  final List<Todo> todos;
  final Function(Todo, bool) onCheckboxChanged;
  final Function(Todo) onRemove;
  final Function(Todo) onUndoRemove;

  TodoList({
    @required this.todos,
    @required this.onCheckboxChanged,
    @required this.onRemove,
    @required this.onUndoRemove,
  })
      : super(key: AppKeys.todoList);

  @override
  Widget build(BuildContext context) {
    return new AppLoading(builder: (context, loading) {
      return loading
          ? new Center(
              key: AppKeys.todosLoading,
              child: new CircularProgressIndicator(
                key: AppKeys.statsLoading,
              ))
          : new Container(
              child: new ListView.builder(
                key: AppKeys.todoList,
                itemCount: todos.length,
                itemBuilder: (BuildContext context, int index) {
                  final todo = todos[index];

                  return new TodoItem(
                    todo: todo,
                    onDismissed: (direction) {
                      _removeTodo(context, todo);
                    },
                    onTap: () => _onTodoTap(context, todo),
                    onCheckboxChanged: (complete) {
                      onCheckboxChanged(todo, complete);
                    },
                  );
                },
              ),
            );
    });
  }

  void _removeTodo(BuildContext context, Todo todo) {
    onRemove(todo);

    Scaffold.of(context).showSnackBar(new SnackBar(
        key: AppKeys.snackbar,
        duration: new Duration(seconds: 2),
        backgroundColor: Theme.of(context).backgroundColor,
        content: new Text(
          AppLocalizations.of(context).todoDeleted(todo.task),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
        action: new SnackBarAction(
          label: AppLocalizations.of(context).undo,
          onPressed: () => onUndoRemove(todo),
        )));
  }

  void _onTodoTap(BuildContext context, Todo todo) {
    Navigator.of(context).push(
      new MaterialPageRoute(
        builder: (_) {
          return new TodoDetails(
            id: todo.id,
          );
        },
      ),
    ).then((removedTodo) {
      if (removedTodo != null) {
        Scaffold.of(context).showSnackBar(
              new SnackBar(
                key: AppKeys.snackbar,
                duration: new Duration(seconds: 2),
                backgroundColor: Theme.of(context).backgroundColor,
                content: new Text(
                  AppLocalizations.of(context).todoDeleted(todo.task),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                action: new SnackBarAction(
                  label: AppLocalizations.of(context).undo,
                  onPressed: () {
                    onUndoRemove(todo);
                  },
                ),
              ),
            );
      }
    });
  }
}
