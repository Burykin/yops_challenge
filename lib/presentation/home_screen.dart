import 'package:yops_challenge/containers/action_selector.dart';
import 'package:yops_challenge/containers/active_tab.dart';
import 'package:yops_challenge/containers/filter_selector.dart';
import 'package:yops_challenge/containers/filtered_todos.dart';
import 'package:yops_challenge/containers/stats.dart';
import 'package:yops_challenge/containers/tab_selector.dart';
import 'package:yops_challenge/models/models.dart';
import 'package:yops_challenge/localization.dart';
import 'package:yops_challenge/presentation/filter_button.dart';
import 'package:flutter/material.dart';
import 'package:yops_challenge/src.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new ActiveTab(
      builder: (BuildContext context, AppTab activeTab) {
        return new Scaffold(
          appBar: new AppBar(
            title: new Text(BuiltReduxLocalizations.of(context).appTitle),
            actions: [
              new FilterSelector(
                builder: (context, vm) {
                  return new FilterButton(
                    visible: activeTab == AppTab.todos,
                    activeFilter: vm.activeFilter,
                    onSelected: vm.onFilterSelected,
                  );
                },
              ),
              new ExtraActionSelector()
            ],
          ),
          body: activeTab == AppTab.todos ? new FilteredTodos() : new Stats(),
          floatingActionButton: new FloatingActionButton(
            key: AppKeys.addTodoFab,
            onPressed: () {
              Navigator.pushNamed(context, Routes.addTodo);
            },
            child: new Icon(Icons.add),
            tooltip: AppLocalizations.of(context).addTodo,
          ),
          bottomNavigationBar: new TabSelector(),
        );
      },
    );
  }
}
