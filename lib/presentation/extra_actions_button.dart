import 'package:yops_challenge/models/models.dart';
import 'package:flutter/material.dart';
import 'package:yops_challenge/src.dart';

class ExtraActionsButton extends StatelessWidget {
  final PopupMenuItemSelected<ExtraAction> onSelected;
  final bool allComplete;

  ExtraActionsButton({
    this.onSelected,
    this.allComplete = false,
    Key key,
  })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new PopupMenuButton<ExtraAction>(
      key: AppKeys.extraActionsButton,
      onSelected: onSelected,
      itemBuilder: (BuildContext context) => <PopupMenuItem<ExtraAction>>[
            new PopupMenuItem<ExtraAction>(
              key: AppKeys.toggleAll,
              value: ExtraAction.toggleAllComplete,
              child: new Text(allComplete
                  ? AppLocalizations.of(context).markAllIncomplete
                  : AppLocalizations.of(context).markAllComplete),
            ),
            new PopupMenuItem<ExtraAction>(
              key: AppKeys.clearCompleted,
              value: ExtraAction.clearCompleted,
              child:
                  new Text(AppLocalizations.of(context).clearCompleted),
            ),
          ],
    );
  }
}
