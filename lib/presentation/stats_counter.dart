import 'package:yops_challenge/containers/app_loading.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:yops_challenge/src.dart';

class StatsCounter extends StatelessWidget {
  final int numActive;
  final int numCompleted;

  StatsCounter({
    @required this.numActive,
    @required this.numCompleted,
  });

  @override
  Widget build(BuildContext context) {
    return new AppLoading(builder: (context, loading) {
      return loading
          ? new Center(
              key: AppKeys.statsLoading,
              child: new CircularProgressIndicator(
                key: AppKeys.statsLoading,
              ))
          : new Center(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  new Padding(
                    padding: new EdgeInsets.only(bottom: 8.0),
                    child: new Text(
                      AppLocalizations.of(context).completedTodos,
                      style: Theme.of(context).textTheme.title,
                    ),
                  ),
                  new Padding(
                    padding: new EdgeInsets.only(bottom: 24.0),
                    child: new Text(
                      '$numCompleted',
                      key: AppKeys.statsNumCompleted,
                      style: Theme.of(context).textTheme.subhead,
                    ),
                  ),
                  new Padding(
                    padding: new EdgeInsets.only(bottom: 8.0),
                    child: new Text(
                      AppLocalizations.of(context).activeTodos,
                      style: Theme.of(context).textTheme.title,
                    ),
                  ),
                  new Padding(
                    padding: new EdgeInsets.only(bottom: 24.0),
                    child: new Text(
                      "$numActive",
                      key: AppKeys.statsNumActive,
                      style: Theme.of(context).textTheme.subhead,
                    ),
                  )
                ],
              ),
            );
    });
  }
}
