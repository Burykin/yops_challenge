library serializers;

import 'package:yops_challenge/models/models.dart';
import 'package:built_value/serializer.dart';
import 'package:built_collection/built_collection.dart';

part 'serializers.g.dart';

@SerializersFor(const [
  AppTab,
  ExtraAction,
  VisibilityFilter,
  AppState,
  Todo,
])
final Serializers serializers = _$serializers;
